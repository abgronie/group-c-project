# Online Library Management System

The Online Library Management System is software that provides the ability to find books, manage books, 
track borrowed books, manage fines and bills all in one place. It helps the librarian manage the books 
and books borrowed by members and automates most of the library activities. It increases efficiency and
saves time and effort for both the user and the
librarian.

#  Functional Requirements of the Online Library Management System

The system will enable the librarian to add and remove new members

The user will be able to search for books based on title, publication date, author, etc., and find their 
location in the library

Users will be able to request, reserve, or renew a book

The system will allow the librarian to add and manage the books

The system notification function will notify users and the librarian about books due for return

The system will determine fines for overdue books on their return

User - The user will be able to log in, view the catalog, search for books, checkout, and reserve,
renew and return a book

Librarian - The librarian will register new users, add and maintain the books, collect fines for overdue 
books, and issues books to users who need them.

System - The system is the Online Library Management System itself. It will be able to keep track of the
borrowed books and send notifications to the user and librarian about the overdue books


# Nonfunctional requirements

Usability
The User Interface will be simple enough for everyone to understand and get the relevant information 
without any special training

Accuracy
The data stored about the books and the fines calculated will be correct, consistent, and reliable.

Availability
The System will be available for the duration when the library operates. The system will have a fast response 
time.

Maintainability
The software will be easily maintained with the possibility of adding new features and making changes. Portability
will not be overlooked by the software development.

# Software Requirements

A server running Windows Server/Linux OS

A multi-threading capable backend language like python

Front-end technologies that include HTML, CSS and JavaScript

MySQL will be the Database Management System 

Minimum processor requirement is a duo core processor because it is fast enough, reliable and stable

Minimum of 1 GB of RAM memory requirement to provide fast reading and writing capabilities and will in turn support
in processing
